app.factory('Auth', [
  '$rootScope', 'localStorageService', '$location', 'azp@oauth3.org'
, function($rootScope, localStorageService, $location, Oauth3) {

  var dapSession = 'dap-session';
  var dapSessions = 'dap-sessions';

  var Auth = {
    isLoggedIn: function () {
      Auth.restore();

      return Auth.session || false;
    },
    getProfile: function (profile) {
      Auth.restore();

      return Auth.session || false;
    },
    getActiveSessions: function () {
      Auth.restore();

      return Auth.sessions || false;
    }
  , add: function (session) {
      var obj = JSON.parse(localStorage.getItem(dapSessions) || 'null') || {};
      var dapName = 'dap-' + session.subject + '|' + session.issuer;

      /*
      Object.keys(Auth.session).forEach(function (key) {
        delete Auth.session[key];
      });
      Object.keys(session).forEach(function (key) {
        Auth.session[key] = session[key];
      });
      */
      Auth.session = session;
      Auth.sessions.push(session);

      localStorage.setItem(dapName, JSON.stringify(session));
      localStorage.setItem(dapSession, dapName);
      obj[dapName] = Date.now();
      localStorage.setItem(dapSessions, JSON.stringify(obj));
    }
  , restore: function () {
      var dapName = localStorage.getItem(dapSession);
      Auth.sessions.length = 0; // don't overwrite with a new array, keep original references

      (Object.keys(JSON.parse(localStorage.getItem(dapSessions) || 'null') || {})).forEach(function (name) {
        var session = JSON.parse(localStorage.getItem(name) || 'null');

        if (session) {
          session.email = session.subject;
        }
        if (!session.issuer) {
          console.error(session);
          throw new Error('restored session without audience');
        }

        if (dapName === name) {
          Auth.session = session;
        }

        Auth.sessions.push(session);
      });

      if (Auth.session) {
        Auth.select(Auth.session);
      }
      return Auth.session;
    }
  , get: function (session) {
      if (!session) {
        return $q.resolve(null);
      }

      if (!session.issuer) {
        throw new Error("session doesn't have an issuer");
      }

      var name = session.token.sub + '@' + session.token.iss;
      var promise;
      var sess;

      if (!Auth._oauth3s[name]) {
        sess = Oauth3.create(window.location);
        promise = Auth._oauth3s[name] = sess.init({
          location: location
        , issuer: session.issuer
        , audience: session.audience || session.issuer
        , session: session
        }).then(function () {
          return sess;
        });
      } else {
        promise = Oauth3.PromiseA.resolve(Auth._oauth3s[name]);
      }

      return promise;
    }
  , select: function (session) {
      return Auth.get(session).then(function (oauth3) {
        var dapName = 'dap-' + session.subject + '|' + session.issuer;
        localStorage.setItem(dapSession, dapName);

        Auth.session = session;
        Auth.oauth3 = oauth3;
      });
    }
  , signOut: function () {
      var session = Auth.session;
      var dapName = 'dap-' + session.subject + '|' + session.issuer;
      // TODO logout url should be created upon login and remain fixed throughout the duration of the session (or on session restoration)
      return Auth.oauth3.logout().then(function () {
        var obj = JSON.parse(localStorage.getItem(dapSessions) || '{}');
        delete obj[dapName];
        var newDapName = Object.keys(obj).sort(function (a, b) { return obj[a] - obj[b]; })[0];

        localStorage.setItem(dapSession, newDapName);
        localStorage.setItem(dapSessions, JSON.stringify(obj));
        localStorage.removeItem(dapName);

        if (!newDapName) {
          localStorage.removeItem(dapSession);
        }

        return Auth.restore();
      });
      // localStorage.clear();
    }
  , _oauth3s: {}
  , sessions: []
  , session: null
  , oauth3: null
  };

  Auth.oauth3 = Oauth3.create(window.location);

  return Auth;
}]);
