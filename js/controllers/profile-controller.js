app.controller('profileCtrl', [
  '$scope', '$timeout', 'Auth', '$location', 'localStorageService', '$rootScope', 'azp@oauth3.org', '$stateParams'
, function ($scope, $timeout, Auth, $location, localStorageService, $rootScope, Oauth3, $stateParams) {

  var vm = this;

  vm.independentIssuer = false;
  // TODO reuse most recent issuer?
  vm.newOauth3 = Oauth3.create(location);
  vm.timers = {};
  vm.defaultIssuer = 'provider.' + location.host.replace(/^cloud\./, '');

  vm.Auth = Auth;
  vm.session = Auth.session;
  vm.sessions = Auth.sessions;

  vm.showAdvanced = true;

  vm.toggleAdvanced = function () {
    vm.showAdvanced = !vm.showAdvanced;
    vm.independentIssuer = !vm.independentIssuer;
  };

  vm.notification = true;

  vm._setSubject = function (subject) {
    vm.currentSubject = vm.newSubject;
    subject = subject || vm.newSubject;
    var issuer = subject.replace(/.*@/, '');
    if (vm.independentIssuer) {
      return $timeout(function () { return; }, 0);
    }
    return Oauth3.discover(issuer, { client_uri: Oauth3.clientUri(location) }).then(function (deets) {
      return vm._setIssuer(issuer);
    }, function () {
      // ignore error
    });
  };

  vm.setSubject = function (subject) {
    $timeout.cancel(vm.timers.subject);
    vm.timers.subject = $timeout(function () {
      vm._setSubject(subject);
    }, 300);
  };

  vm._setIssuer = function (url) {
    vm.spinner = true;
    url = (url || vm.newIssuer).replace(/.*@/, '');
    if (!url) {
      url = vm.defaultIssuer;
    }

    return Oauth3.discover(url, { client_uri: Oauth3.clientUri(location) }).then(function (deets) {
      vm.currentIssuer = url;
      vm.issuerName = url;

      return vm.newOauth3.setIdentityProvider(url).then(function (deets) {
        vm.newOauth3.setResourceProvider(url);
        vm.spinner = false;
        // TODO add icon and name to directives
      });
    }, function () {
      console.log("oauth3 discover timeout: No dice, no change for '" + url + "'");
      vm.spinner = false;
    });
  };

  vm.setIssuer = function (url) {
    $timeout.cancel(vm.timers.issuer);
    vm.timers.issuer = $timeout(function () {
      vm._setIssuer(url);
    }, 300);
  };

  vm.setAudience = function (url) {
    url = url || vm.audienceUrl;
    vm.audienceName = url;
    vm.newOauth3.setResourceProvider(url);
  };

  vm.selectSession = function (session) {
    vm.xauth = true;
    vm.session = session;
    return Auth.select(session).then(function (oauth3) {
      vm.xauth = false;
    });
  };

  vm.instaauth = function () {
    return vm._setSubject().then(function () {
      return vm._setIssuer().then(function () {
        return vm.auth();
      });
    });
  };

  vm.auth = function () {
    var subject = vm.currentSubject;
    var issuer = vm.issuerName;
    return vm.newOauth3.authenticate({
      subject: subject
    , scope: [ 'domains@oauth3.org', 'domains', 'dns@oauth3.org', 'dns', 'www@daplie.com' ]
    }).then(function (session) {
      session.subject = subject;
      session.issuer = issuer;
      Auth.add(session);
      if ($rootScope.redirectedURL === '/splash-page') {
        $location.path('/home');
      } else {
        $location.path('/' + $rootScope.redirectedURL);
      }
    }, function (err) {
      console.error('auth error');
      console.error(err);
    });
  };

  vm.newIssuer = vm.defaultIssuer;
  vm.setIssuer(vm.defaultIssuer);

  vm.getSession = function() {
    return Auth.select(Auth.session);
  };

  vm.initListLoggedInProfiles = function () {
    vm.activeProfiles = Auth.getActiveSessions();
  };

  vm.signIn = function () {
    vm.auth();
  };

  vm.masterLogOut = function () {
    localStorage.clear();
    $location.path('/splash-page');
  };

  vm.signOut = function () {
    vm.getSession().then(function(){
      // TODO the sign-out url for each account should be fixed.
      return Auth.signOut().then(function () {
        if (Auth.sessions.length === 0) {
          $location.path('/splash-page');
          return;
        }
        window.alert("You are still logged in with other accounts.");
      });
    });
  };

  vm.Profile = {};
  vm.Profile.update = function (a) {
    console.log('Click click click!!!');
    var pkg = Auth.oauth3.pkg('issuer@oauth3.org');

    return pkg.update({
      displayName: a.displayName
    , avatarUrl: a.avatarUrl
    , firstName: a.firstName
    , lastName: a.lastName
    //, names: a.names
    , primaryEmail: a.primaryEmail // TODO make a combobox of available emails (and require confirm before making primary)
    , primaryPhone: a.primaryPhone
    }).then(function (result) {
      window.alert(JSON.stringify(result));
      // TODO use iframe to initiate download?
      vm.account = result.data;
    });
  };
  vm.Profile.get = function () {
    var pkg = Auth.oauth3.pkg('issuer@oauth3.org');

    return pkg.get().then(function (result) {
      console.log(result.data);
      vm.account = result.data;
      vm.profile = result.data;
    });
  };

  vm.Profile.get();

}]);
