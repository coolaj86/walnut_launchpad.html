app.controller('emailCtrl', [
  '$scope', '$q', 'Auth', 'azp@oauth3.org', '$timeout'
, function ($scope, $q, Auth, Oauth3, $timeout) {
    var vm = this;

    vm.getSettings = function () {
      var pkg = Auth.oauth3.pkg('email@daplie.com');
      return pkg.settings().then(function (resp) {
        vm.settings.service = 'mailgun.org';
        vm.settings = resp.data;
      });
    };

    vm.getSettings();
  }
]);
