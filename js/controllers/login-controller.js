app.directive('referrerNotification', [function () {
  return {
    restrict: 'EA',
    templateUrl: '/templates/widgets/referrer.html',
  };
}]);

app.controller('loginCtrl', [
  '$scope', '$timeout', 'Auth', '$location', 'localStorageService', '$rootScope', 'azp@oauth3.org', '$stateParams'
, function ($scope, $timeout, Auth, $location, localStorageService, $rootScope, Oauth3, $stateParams) {

  var vm = this;

  vm.independentIssuer = false;
  // TODO reuse most recent issuer?
  vm.newOauth3 = Oauth3.create(location);
  vm.timers = {};
  vm.defaultIssuer = 'provider.' + location.host.replace(/^cloud\./, '');

  vm.Auth = Auth;
  vm.session = Auth.session;
  vm.sessions = Auth.sessions;

  vm.showAdvanced = true;

  vm.hideReferrerNotification = 'hidden';
  vm.referrerInit = function () {
    vm.referrerAlert = {};
    if ($location.hash() !== '') {
      var str = $location.hash();
      var domain = str.split('=')[1];
      vm.referrerAlert.title = domain + " has been created. What's next?";
      vm.referrerAlert.msg = "Do this next... Or do this... Or this...";
      if (str.indexOf('referrer') === 0) {
        vm.hideReferrerNotification = '';
        vm.closeReferrerModal = function () {
          var popup = document.querySelector('.referrer-pop-up');
          popup.style.display = 'none';
          $location.url($location.path());
        };
        vm.sendToShares = function (){
          $location.path('/website');
          vm.hideReferrerNotification = 'hidden';
        };
      }
    }
  };

  vm.toggleAdvanced = function () {
    vm.showAdvanced = !vm.showAdvanced;
    vm.independentIssuer = !vm.independentIssuer;
  };

  vm.notification = true;

  vm._setSubject = function (subject) {
    vm.currentSubject = vm.newSubject;
    subject = subject || vm.newSubject;
    var issuer = subject.replace(/.*@/, '');
    if (vm.independentIssuer) {
      return $timeout(function () { return; }, 0);
    }
    return Oauth3.discover(issuer, { client_uri: Oauth3.clientUri(location) }).then(function (deets) {
      return vm._setIssuer(issuer);
    }, function () {
      // ignore error
    });
  };

  vm.setSubject = function (subject) {
    $timeout.cancel(vm.timers.subject);
    vm.timers.subject = $timeout(function () {
      vm._setSubject(subject);
    }, 300);
  };

  vm._setIssuer = function (url) {
    vm.spinner = true;
    url = (url || vm.newIssuer).replace(/.*@/, '');
    if (!url) {
      url = vm.defaultIssuer;
    }

    return Oauth3.discover(url, { client_uri: Oauth3.clientUri(location) }).then(function (deets) {
      vm.currentIssuer = url;
      vm.issuerName = url;

      return vm.newOauth3.setIdentityProvider(url).then(function (deets) {
        vm.newOauth3.setResourceProvider(url);
        vm.spinner = false;
        // TODO add icon and name to directives
      });
    }, function () {
      console.log("oauth3 discover timeout: No dice, no change for '" + url + "'");
      vm.spinner = false;
    });
  };

  vm.setIssuer = function (url) {
    $timeout.cancel(vm.timers.issuer);
    vm.timers.issuer = $timeout(function () {
      vm._setIssuer(url);
    }, 300);
  };

  vm.setAudience = function (url) {
    url = url || vm.audienceUrl;
    vm.audienceName = url;
    vm.newOauth3.setResourceProvider(url);
  };

  vm.selectSession = function (session) {
    vm.xauth = true;
    vm.session = session;
    return Auth.select(session).then(function (oauth3) {
      vm.xauth = false;
    });
  };

  vm.instaauth = function () {
    return vm._setSubject().then(function () {
      return vm._setIssuer().then(function () {
        return vm.auth();
      });
    });
  };

  vm.auth = function () {
    var subject = vm.currentSubject;
    var issuer = vm.issuerName;
    return vm.newOauth3.authenticate({
      subject: subject
    , scope: [ 'domains@oauth3.org', 'domains', 'dns@oauth3.org', 'dns', 'www@daplie.com' ]
    }).then(function (session) {
      session.subject = subject;
      session.issuer = issuer;
      Auth.add(session);
      if (Auth.sessions.length === 1) {
        if ($rootScope.urlCrumbs[0].path === '/splash-page') {
          $location.path('/home');
        } else {
          window.location.replace($rootScope.urlCrumbs[0].absUrl);
        }
      } else {
        location.reload();
        // window.location.replace($rootScope.urlCrumbs[$rootScope.urlCrumbs.length -1].absUrl);
      }
    }, function (err) {
      console.error('auth error');
      console.error(err);
    });
  };

  vm.newIssuer = vm.defaultIssuer;
  vm.setIssuer(vm.defaultIssuer);

  vm.getSession = function() {
    return Auth.select(Auth.session);
  };

  vm.initListLoggedInProfiles = function () {
    vm.activeProfiles = Auth.getActiveSessions();
  };

  vm.signIn = function () {
    vm.auth();
  };

  vm.masterLogOut = function () {
    localStorage.clear();
    $location.path('/splash-page');
  };

  vm.signOut = function () {
    vm.getSession().then(function(){
      // TODO the sign-out url for each account should be fixed.
      return Auth.signOut().then(function () {
        if (Auth.sessions.length === 0) {
          $location.path('/splash-page');
          return;
        }
        window.alert("You are still logged in with other accounts.");
      });
    });
  };
}]);
