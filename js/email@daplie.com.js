;(function (exports) {
'use strict';

var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

OAUTH3._pkgs['email@daplie.com'] = {
  settings: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'GET'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/email@daplie.com/acl/settings/'
    , session: session
    }).then(function (result) {
      return result;
    });
  }
};

}('undefined' !== typeof exports ? exports : window));
