app.directive('achievementDirective', function() {
  return {
    templateUrl: "templates/widgets/achievement-widget.html",
    controller: 'homeCtrl',
    controllerAs: 'vm'
  };
});
