;(function (exports) {
'use strict';

var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

OAUTH3._pkgs['www@daplie.com'] = {
  add: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/sites/' + opts.hostname
        + '?' + OAUTH3.utils.query.stringify({
            tld: opts.tld, sld: opts.sld, sub: opts.sub, unzip: opts.unzip
          , strip: opts.strip, path: opts.path
          })
    , session: session
    , multipart: opts.multipart // special property to be figured out by browser request code
    , progress: opts.progress
    }).then(function (result) {
      return result;
    });
  }
, archiveUrl: function (opts) {
    var providerUri = opts.audience;

    return OAUTH3.url.normalize(providerUri.replace(/api\./, 'assets.'))
      + '/assets/www@daplie.com/acl/archives/' + opts.hostname
      + '?' + OAUTH3.utils.query.stringify({ tld: opts.tld, sld: opts.sld, sub: opts.sub, strip: opts.strip, path: opts.path })
      ;
  }
, listShares: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    // TODO needs a way to have api and assets for audience
    return OAUTH3.request({
      method: 'GET'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/shares/' + (opts.domain || opts.hostname)
        + '?' + OAUTH3.utils.query.stringify({ tld: opts.tld, sld: opts.sld, sub: opts.sub, path: opts.path })
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, removeShare: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    // TODO needs a way to have api and assets for audience
    return OAUTH3.request({
      method: 'DELETE'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/shares/' + (opts.domain || opts.hostname) + '/' + (opts.challenge || opts.token)
        + '?' + OAUTH3.utils.query.stringify({ tld: opts.tld, sld: opts.sld, sub: opts.sub, path: opts.path })
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, invite: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    // TODO needs a way to have api and assets for audience
    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/shares/' + (opts.domain || opts.hostname)
        + '?' + OAUTH3.utils.query.stringify({
            tld: opts.tld
          , sld: opts.sld
          /*, sub: opts.sub*/
          , mode: opts.mode
          , path: opts.path
          , email: opts.email
          , comment: opts.comment
          })
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, acceptInvite: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    // TODO needs a way to have api and assets for audience
    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/shares/accept/' + opts.token
        + '?' + OAUTH3.utils.query.stringify({ tld: opts.tld, sld: opts.sld, sub: opts.sub })
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, download: function (opts) {
    var session = opts.session;
    var me = this;

    // TODO needs a way to have api and assets for audience
    return OAUTH3.request({
      method: 'GET'
    , url: me.archiveUrl
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, archive: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;
    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/archives/' + opts.hostname
        + '?' + OAUTH3.utils.query.stringify({ tld: opts.tld, sld: opts.sld, sub: opts.sub, strip: opts.strip, path: opts.path })
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, contents: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'GET'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/fs/' + opts.hostname
        + '?' + OAUTH3.utils.query.stringify({ tld: opts.tld, sld: opts.sld, sub: opts.sub, path: opts.path })
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, contentRange: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'GET'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/fs/' + opts.hostname
        + '?' + OAUTH3.utils.query.stringify({ tld: opts.tld, sld: opts.sld, sub: opts.sub, path: opts.path
          , offset: opts.offset, length: opts.length, json: true })
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, remove: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'DELETE'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/sites/' + opts.hostname
        + '?' + OAUTH3.utils.query.stringify({
            tld: opts.tld, sld: opts.sld, sub: opts.sub
          , path: opts.path, confirm: opts.confirm
          })
    , session: session
    , multipart: opts.multipart // special property to be figured out by browser request code
    }).then(function (result) {
      return result;
    });
  }
, list: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'GET'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/list/'
    , session: session
    }).then(function (result) {
      result.data = result.data && result.data.sites || result.data;

      return result;
    });
  }
, request: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/request/:tld/:sld/:sub'
            .replace(/(:tld)/, opts.tld)
            .replace(/(:sld)/, opts.sld)
            .replace(/(:sub)/, opts.sub || '')
    , session: session
    }).then(function (result) {
      // result.data
      return result;
    });
  }
, claim: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/www@daplie.com/acl/claim/:tld/:sld/:sub'
            .replace(/(:tld)/, opts.tld)
            .replace(/(:sld)/, opts.sld)
            .replace(/(:sub)/, opts.sub || '')
    , session: session
    }).then(function (result) {
      // result.data

      return result;
    });
  }
};

}('undefined' !== typeof exports ? exports : window));
