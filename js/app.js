(function () {
'use strict';

var angular = window.angular;
var OAUTH3 = window.OAUTH3;

var app = window.app = angular.module('launchpad', ['oauth3.org', 'ui.router', 'LocalStorageModule', 'angucomplete-alt', 'ez.fileTree', 'ui.multiselect']);

app.directive('daplieFileChange', function () {
  return {
    restrict: 'A',
    require:"ngModel",
    link: function (scope, element, attrs, ngModel) {
      element.bind('change', function (event) {
        var files = event.target.files;
        ngModel.$setViewValue(files[0]);
        scope.$eval(attrs.daplieFileChange);
      });
    }
  };
});

app.config([
  '$stateProvider', '$urlRouterProvider', 'localStorageServiceProvider', '$urlMatcherFactoryProvider',
  function ($stateProvider, $urlRouterProvider, localStorageServiceProvider, $urlMatcherFactoryProvider) {

  $urlMatcherFactoryProvider.strictMode(false);

  localStorageServiceProvider.setPrefix('launchpad').setStorageType('sessionStorage');

  $urlRouterProvider.otherwise('/splash-page');

  $stateProvider
  .state('splash-page', {
    data: { requiresLogin: false, session: null },
    url: '/splash-page',
    templateUrl: '/templates/splash-page.html',
    controller: 'loginCtrl as vm'
  })
  .state('app',{
    data: { requiresLogin: true, session: null },
    url: '/',
    controller: 'loginCtrl as vm',
    views: {
      'header': {
        templateUrl: '/templates/partials/header.html',
      },
      'menu': {
        templateUrl: '/templates/partials/menu.html'
      },
      'content': {
        templateUrl: '/templates/home.html'
      }
    }
  })
  .state('app.home', {
    url: 'home',
    views: {
      'content@': {
        templateUrl: 'templates/home.html',
        controller: 'loginCtrl as vm'
      }
    }
  })
  .state('app.bolt', {
    url: 'bolt',
    views: {
      'content@': {
        templateUrl: 'templates/bolt.html',
        controller: 'boltCtrl as vm',
      }
    }
  })
  .state('app.files', {
    url: 'files',
    views: {
      'content@': {
        templateUrl: 'templates/files.html',
        controller: 'fileCtrl as vm',
      }
    }
  })
  .state('app.contacts', {
    url: 'contacts',
    views: {
      'content@': {
        templateUrl: 'templates/contacts.html',
        controller: 'contactCtrl as vm',
      }
    }
  })
  .state('app.music', {
    url: 'music',
    views: {
      'content@': {
        templateUrl: 'templates/music.html',
        controller: 'musicCtrl as vm',
      }
    }
  })
  .state('app.email', {
    url: 'email',
    views: {
      'content@': {
        templateUrl: 'templates/email.html',
        controller: 'emailCtrl as vm',
      }
    }
  })
  .state('app.website', {
    url: 'website',
    views: {
      'content@': {
        templateUrl: 'templates/website.html',
        controller: 'websiteCtrl as vm',
      }
    }
  })
  .state('app.dns', {
    url: 'dns',
    views: {
      'content@': {
        templateUrl: 'templates/dns.html',
        controller: 'dnsCtrl',
      }
    }
  })
  .state('app.applications', {
    url: 'apps',
    views: {
      'content@': {
        templateUrl: 'templates/applications.html',
        controller: '',
      }
    }
  })
  .state('app.devices', {
    url: 'devices',
    views: {
      'content@': {
        templateUrl: 'templates/devices.html',
        controller: '',
      }
    }
  })
  .state('app.account-settings', {
    url: 'account-settings',
    views: {
      'content@': {
        templateUrl: 'templates/account-settings.html',
        controller: 'profileCtrl as vm',
      }
    }
  });
}]);

app.run(['$rootScope', '$state', 'Auth', '$location', function($rootScope, $state, Auth, $location) {
  $rootScope.urlCrumbs = [];
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    var requiresLogin = toState.data.requiresLogin;
    var Crumbs = new Object();
    Crumbs = {
      absUrl: $location.$$absUrl,
      url: $location.$$url,
      path: $location.$$path,
      params: $location.$$search,
      toPath: toState.url,
      fromPath: fromState.url
    };
    $rootScope.urlCrumbs.push(Crumbs);
    if (requiresLogin && !Auth.isLoggedIn()) {
      event.preventDefault();
      $state.go('splash-page', { 'toState': toState.name });
    }
  });
}]);
}());
