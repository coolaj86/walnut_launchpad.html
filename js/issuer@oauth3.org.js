(function (exports) {
'use strict';

var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

OAUTH3._pkgs['issuer@oauth3.org'] = {
  update: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/issuer@oauth3.org/acl/profile/'
    , session: session
    , data: {
        displayName: opts.displayName
      , avatarUrl: opts.avatarUrl
      , firstName: opts.firstName
      , lastName: opts.lastName
      , primaryEmail: opts.primaryEmail
      , primaryPhone: opts.primaryPhone
      }
    }).then(function (result) {
      return result;
    });
  }
, get: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'GET'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/issuer@oauth3.org/acl/profile/'
    , session: session
    }).then(function (result) {
      return result;
    });
  }
, requestContact: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/issuer@oauth3.org/acl/contact_nodes/'
    , session: session
    , data: {
        type: opts.type
      , node: opts.node
      }
    }).then(function (result) {
      return result;
    });
  }
, verifyContact: function (opts) {
    var providerUri = opts.audience;
    var session = opts.session;

    return OAUTH3.request({
      method: 'POST'
    , url: OAUTH3.url.normalize(providerUri)
        + '/api/issuer@oauth3.org/acl/contact_nodes/'
    , session: session
    , data: {
        type: opts.type
      , node: opts.node
      , challenge: opts.challenge
      }
    }).then(function (result) {
      return result;
    });
  }
};

}('undefined' !== typeof exports ? exports : window));
