OAuth3 Web App
--------------

Prototype OAuth3 Web App.

No build tools or fancy web server required.

Clone and load the HTML. That's all.

```bash
npm install -g serve-https
git clone git@git.coolaj86.com:coolaj86/walnut_launchpad.html.git

# install oauth3 to /assets/oauth3.org
pushd walnut_launchpad
bash ./install.sh

serve-https
```

<https://localhost.example.com:8443>
